#!/bin/bash

# Schritt 1: Lokales Git-Repository erstellen

git branch pages
git checkout pages
git add .

git init


read -p "Gib die Commit-Nachricht ein: " commit_message


git commit -m "$commit_message"

git push --set-upstream origin pages

git checkout main 

echo "Das Repository wurde auf pages hochgeladen."
